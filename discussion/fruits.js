// Aggregation in MongoDB

// Creating fruits documents
db.fruits.insertMany([
    {
        name : "Apple",
        color : "Red",
        stock : 20,
        price : 40,
        supplier_id : 1,
        onSale : true,
        origin : ["Philippines", "US"]
    },
    {
        name : "Banana",
        color : "Yellow",
        stock : 15,
        price : 20,
        supplier_id : 2,
        onSale : true,
        origin : ["Philippines", "Ecuador"]
    },
    {
        name : "Kiwi",
        color : "Green",
        stock : 25,
        price : 50,
        supplier_id : 1,
        onSale : true,
        origin : ["US", "China"]
    },
    {
        name : "Mango",
        color : "Yellow",
        stock : 10,
        price : 120,
        supplier_id : 2,
        onSale : false,
        origin : ["Philippines", "India"]
    }
]);

// Using the aggregate method
/*
    $match
        Syntax:
          { $match: {field: value} }

    $group
        Syntax:
          { $group: { _id: "value", fieldResult: "valueResult"} }

    Syntax:
        db.collectionName.aggregate([
            { $match: {fieldA: value} },
            { $group: { _id: "$fieldB", fieldResult: "valueResult"} }
        ])
*/

// Getting items on sale and the total stocks from suppliers
db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: { _id: "$supplier_id", total: { $sum: "$stock"}} }
]);


// Field projection with aggregation
/*
    $project
    Syntax:
        { $project : { field: 1/0}}
*/

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: { _id: "$supplier_id", total: { $sum: "$stock"}} },
    { $project: { _id: 0 } }
]);

// Sorting aggregated results
/*
    $sort
    Syntax:
        { $sort { field: 1/-1 } }
*/
db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: { _id: "$supplier_id", total: { $sum: "$stock"}} },
    { $sort: { total: -1 } }
]);


// Aggregating results based on array fields
/*
    $unwind
    Syntax:
        { $unwind: field }
*/

db.fruits.aggregate([
    { $unwind: "$origin" }
]);

// Displays fruit documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
    { $unwind: "$origin" },
    { $group: { _id : "$origin", kinds : { $sum : 1} } }
]);

db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "0987654321"
});


// Schema Design
/*
    Guidelines:
        - Schema design/data modelling is an important feature when creating databases.
        - MongoDB documents can be categorized into normalized and de-normalized/embedded data.
        - Normalized data refers to a data structure where documents are referred to each other using their ids for related pieces of information.
        - De-normalized data/embedded data refers to a data structure where related pieces of information is added to a document as an embedded object.
        - Both data structures are common practice but each of them have their pros and their cons.
        - Normalized data makes it easier to read information because separate documents can be retrieved but in terms of querying results, it performs slower compared to embedded data due to having to retrieve multiple documents at the same time.
        - This approach is recommended for data structures where pieces of information are commonly operated on/changed.
        - De-normalized data/embedded data makes it easier to query documents and has a faster performance because only one query needs to be done in order to retrieve documents. However, if the data structure becomes too complex it makes it more difficult to manipulate and access information.
        - This approach is recommended for data structures where pieces of information are commonly retrieved and rarely operated on/changed.

*/


// One-To-one relationship
// Creates an id and stores it in the variable owner for use in document creations
var owner = ObjectId();

// Creates an "owner" document that uses the generated id
db.owners.insert({
        _id: owner,
        name: "John Smith",
        contact: "09871234567"
});

/*
{
    "_id" : ObjectId("63e271bf37885eb733aaa6fa"),
    "name" : "John Smith",
    "contact" : "09871234567"
}
*/

// Create suppliers document
db.suppliers.insert({
        name: "ABC Fruits",
        contact: "1234567890",
        owner_id: "63e271bf37885eb733aaa6fa"
});

// One-To-Few relationship

db.suppliers.insert({
        name: "DEF Fruits",
        contact: "1234567890",
        addresses: [
            { street: "123 San Jose St", city: "Manila" },
            { street: "367 Gil Puyat", city: "Makati" }
        ]
});

// One-To-Many relationship
var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();


db.suppliers.insert({
        _id : supplier,
        name : "GHI Fruits",
        contact: "1234567890",
        branches: [
            branch1
        ]
});

db.branches.insert({
    _id: "63e2766837885eb733aaa6fe",
    name: "BF Homes",
    address: "123 Arcardio Santos St",
    city: "Paranaque",
    supplier_id: "63e2766837885eb733aaa6fd"
});


db.branches.insert({
        _id: "63e2766837885eb733aaa6fe",
        name: "Rizal",
        address: "123 San Jose St",
        city: "Manila",
        supplier_id : "63e2766837885eb733aaa6fd"
});